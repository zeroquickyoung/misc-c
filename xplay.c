#include <stdlib.h>
#include <stdio.h>
#include <X11/Xlib.h>

typedef unsigned long ulong;

typedef struct {
        char  *server_vendor;
        int   width;
        int   height;
        ulong white_pixel;
        ulong black_pixel;
} Monitor;

int main(void)
{
        static int num;
        static Window root_window;
        
        static Display *dpy;
        dpy = XOpenDisplay(NULL);
        if (!dpy) {
                fprintf(stderr, "Can not connect to X server.\n");
                exit(EXIT_FAILURE);
        }

        static Monitor mon;        
        num               = DefaultScreen(dpy);
        mon.width         = DisplayWidth(dpy, num);
        mon.height        = DisplayHeight(dpy, num);
        root_window       = RootWindow(dpy, num);
        mon.white_pixel   = WhitePixel(dpy, num);
        mon.black_pixel   = BlackPixel(dpy, num);
        mon.server_vendor = ServerVendor(dpy);
                        
        fprintf(stdout, "\
Window Number:%d\n\
Window Width :%d\n\
Window Height:%d\n\
White Pixel  :%lu\n\
Black Pixel  :%lu\n\
Server Vendor:%s.\n",
                num, mon.width, mon.height,
                mon.white_pixel, mon.black_pixel,
                mon.server_vendor);
        
        XCloseDisplay(dpy);
        
        return EXIT_SUCCESS;
}
