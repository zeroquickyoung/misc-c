#include <X11/Xlib.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

typedef unsigned int uint;

int main(int argc, char **argv)
{
        Display *dpy;
        Window win, root;
        uint width, height, win_height, win_width, win_border;
        int win_x, win_y, screen_number;

        dpy = XOpenDisplay(NULL);
        if (!dpy) {
                fprintf(stderr, "Can not connect to X Server.\n");
                exit(1);
        }

        screen_number = DefaultScreen(dpy);
        win_width     = DisplayWidth(dpy, screen_number);
        win_height    = DisplayHeight(dpy, screen_number);
        width         = win_width / 2;
        height        = win_height / 2;
        win_border    = atoi(argv[1]);
        root          = RootWindow(dpy, screen_number);
        win_x         = 0;
        win_y         = 0;

        win = XCreateSimpleWindow(dpy, root, win_x, win_y,
                                  width, height, win_border,
                                  BlackPixel(dpy, screen_number),
                                  WhitePixel(dpy, screen_number));

        XMapWindow(dpy, win);
	/*XFlush(dpy);*/
	XSync(dpy, False);
        /*XSync(dpy, True);*/

        sleep(10);
        XCloseDisplay(dpy);

        return 0;        
}
