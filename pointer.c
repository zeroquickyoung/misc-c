#include <stdio.h>

void sayhello();
int add(int x, int y);
int subtract(int x, int y);

int main(void)
{
        /* simple pointer */
        int something = 20;
        int *simpleptr = &something;
        fprintf(stdout, "Pointer to data address: %p,value: %d \nPointerAddress: %p\n",
                simpleptr, *simpleptr, &simpleptr);
        fprintf(stdout, "VariableAddress        : %p,value: %d\n",
                &something, something);
        /* end */
        
        /*** Function Pointer ***/
        /* function pointer */
        void (*sayhelloPtr)() = sayhello;
        fputs("(*sayhelloPtr)()       : ", stdout);
        (*sayhelloPtr)();
        fputs("sayhelloPtr()          : ", stdout);
        sayhelloPtr();
        /* end */
        fputs("sayhello()             : ", stdout);
        sayhello();

        /* function pointer with parameters */
        int (*addPtr) (int, int) = add;
        (*addPtr)(5, 5);
        addPtr(5, 5);
        /* end */

        /* function pointer with parameters and return value */
        int (*subtractPtr) (int, int) = subtract;
        int y = (*subtractPtr)(10, 2);
        fprintf(stdout, "(*subtractPtr)(10, 2)  : %2d\n", y);

        int z = subtractPtr(2, 10);
        fprintf(stdout, "subtractPtr(2, 10)     : %2d\n", z);
        /* end */
        /*** END ***/

        return 0;
}

void sayhello()
{
        fputs("Hello, Program!\n", stdout);
}

int add(int x, int y)
{
        return  x + y;
}

int subtract(int x, int y)
{
        return x - y;
}

/*
* linux code 
typedef struct node {
        struct node * next;
} node;

typedf bool (* remove_fn) (node const *v);

void remove_if(node **head, remove_fn rm)
{
        for (node **cur = head; *curr) {
	        node *entry = *curry;
		if (rm(entry)) {
		        *curr = entry->next;
			free(entry);
		} else {
		        curr = &entry->next;
		}
	}
}
* end of linux code 
*/
