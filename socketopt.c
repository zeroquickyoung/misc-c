int set_nonblocking(int fd)
{
        int nb = 1;
        return ioctl(fd, FIONBIO, &nb);
}

int set_blocking(int fd)
{
        int nb = 0;
        return ioctl(fd, FIONBIO, &nb);
}